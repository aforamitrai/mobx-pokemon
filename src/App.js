import { useEffect } from 'react';
import ProfileList from './components/profiles/ProfileList';
import ProfileMode from './components/profiles/profileModels/ProfileModel';
import { useAppContext } from './store';
function App() {
  const {
    state: { profiles, selectedProfile, page },
    actions: { purgeSelectedProfile, getProfiles, setNextPage },
    dispatch,
  } = useAppContext();

  useEffect(() => {
    getProfiles(page)(dispatch);
    const eventScroll = window.addEventListener('scroll', handleScroll); // attaching scroll event listener
    // unmount listener
    return () => eventScroll;
  }, [page]);

  // handle infinite scroll
  const handleScroll = () => {
    let userScrollHeight = window.innerHeight + window.scrollY;
    let windowBottomHeight = document.documentElement.offsetHeight;
    if (userScrollHeight >= windowBottomHeight) {
      // every time it at end stage increment page number
      setNextPage(page + 1)(dispatch);
      // getProfiles(page)(dispatch);
    }
  };

  return (
    <div className='bg-slate-800 '>
      {/* show profile model when their is profile selected   */}
      {selectedProfile && (
        <ProfileMode profile={selectedProfile} close={purgeSelectedProfile} />
      )}

      <div className={'mx-auto min-h-screen  max-w-7xl'}>
        {/* profile list  */}
        <ProfileList profiles={profiles} />
      </div>
    </div>
  );
}

export default App;
