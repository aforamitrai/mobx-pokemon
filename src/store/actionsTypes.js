export const GET_PROFILES = 'GET_PROFILES';
export const SET_PROFILES = 'SET_PROFILE';
export const PURGE_PROFILE = 'PURGE_PROFILE';
export const SET_NEXT_PAGE = 'SET_NEXT';
