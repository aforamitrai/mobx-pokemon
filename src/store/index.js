import React, { createContext } from 'react';
import request from '../utls/request';
import {
  GET_PROFILES,
  PURGE_PROFILE,
  SET_NEXT_PAGE,
  SET_PROFILES,
} from './actionsTypes';

export const initialState = {
  profiles: [],
  page: 1,
  selectedProfile: null,
};

export const actions = {
  // get user profile on initial locas
  //  @params: {number}

  getProfiles: (page) => async (dispatch) => {
    try {
      const response = await request.get('/character', {
        params: { page: page },
      });
      if (response.data) {
        dispatch({ type: GET_PROFILES, payload: response.data });
      }
    } catch (error) {
      console.log(error);
    }
  },

  // select character, call api for particular characters

  selectProfile: (profileId) => async (dispatch) => {
    try {
      const response = await request.get(`/character/${profileId}`);
      if (response.data) {
        dispatch({ type: SET_PROFILES, payload: response });
      }
    } catch (error) {
      console.log(error);
    }
  },
  purgeSelectedProfile: () => (dispatch) => {
    // deleted selected profile,
    dispatch({ type: PURGE_PROFILE, payload: {} });
  },
  setNextPage: (page) => (dispatch) => {
    // sets next page number
    dispatch({ type: SET_NEXT_PAGE, payload: page });
  },
};

const appContext = createContext({
  state: {},
  dispatch: () => {},
  actions: {},
});

export function reducer(state = initialState, action) {
  console.log(state, 'state');
  switch (action.type) {
    case GET_PROFILES:
      return {
        ...state,
        profiles: [...state.profiles, ...action.payload?.results],
        info: action.payload?.info,
      };

    case SET_PROFILES:
      return {
        ...state,
        selectedProfile: action.payload.data,
      };

    // assign selectedProfile as null
    case PURGE_PROFILE:
      return {
        ...state,
        selectedProfile: null,
      };
    case SET_NEXT_PAGE:
      return { ...state, page: action.payload };
    default:
      return state;
  }
}

const AppContext = React.memo(({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  React.useEffect(() => {
    actions.getProfiles()(dispatch);
  }, []);

  return (
    <appContext.Provider
      value={{
        state,
        dispatch,
        actions: actions,
      }}
    >
      {children}
    </appContext.Provider>
  );
});

export const useAppContext = () => React.useContext(appContext);
export default AppContext;
