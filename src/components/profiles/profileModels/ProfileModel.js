import React from 'react';
import { useAppContext } from '../../../store';

function ProfileMode({ profile, close }) {
  const { dispatch } = useAppContext();
  return (
    <div
      className='fixed z-10 inset-0 overflow-y-auto'
      role='dialog'
      aria-modal='true'
    >
      <div className='flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0'>
        <div
          className='fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity'
          aria-hidden='true'
        ></div>
        <div className='relative inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full'>
          <div className='bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4 relative'>
            <div className='flex justify-center text-center'>
              <div>
                <div
                  className='absolute right-5 z-50'
                  // close models calls , selectedProfile as null
                  onClick={() => close()(dispatch)}
                >
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    className='h-8 w-8'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='pink'
                    strokeWidth={2}
                  >
                    <path
                      strokeLinecap='round'
                      strokeLinejoin='round'
                      d='M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z'
                    />
                  </svg>
                </div>
                <img
                  src={profile.image}
                  alt=''
                  className={
                    [
                      profile.status === 'Alive'
                        ? 'border-green-600 border-8'
                        : 'border-red-600 border-8',
                    ] + ' object-cover h-64 w-6h-64 rounded-full object-center'
                  }
                />
                <div className='text-4xl text-purple-800'>{profile.name}</div>
                <div className='text-2sxl text-purple-800'>
                  {profile.location.name}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProfileMode;
