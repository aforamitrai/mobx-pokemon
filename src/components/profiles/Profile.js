import React from 'react';
import { useAppContext } from '../../store';

function Profile({ profile }) {
  const {
    dispatch,
    actions: { selectProfile },
  } = useAppContext();

  return (
    <div
      className='cursor-pointer bg-[#6539c0] py-2 px-3 rounded-xl hover:bg-[#4f2c93]'
      // select selected profile api
      onClick={() => selectProfile(profile.id)(dispatch)}
    >
      <div className='flex items-center space-x-2'>
        <div>
          <img
            src={profile?.image}
            alt={profile?.name}
            className={
              [
                profile.status === 'Alive'
                  ? 'border-green-600 border-4'
                  : 'border-red-600 border-4',
              ] + ' h-24 w-24 rounded-full object-cover shadow-inner'
            }
          />
        </div>
        <div className='text-white'>
          <div className=''>{profile?.name}</div>
          {/* locations  */}
          <div className='text-xs flex items-center space-x-1 py-1'>
            <div>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                class='h-4 w-4'
                fill='none'
                viewBox='0 0 24 24'
                stroke='pink'
                stroke-width='2'
              >
                <path
                  stroke-linecap='round'
                  stroke-linejoin='round'
                  d='M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z'
                />
                <path
                  stroke-linecap='round'
                  stroke-linejoin='round'
                  d='M15 11a3 3 0 11-6 0 3 3 0 016 0z'
                />
              </svg>
            </div>
            <div>{profile?.location.name}</div>
          </div>
          {/* origin  */}
          <div className='text-xs flex items-center space-x-1'>
            <div>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                class='h-4 w-4'
                fill='none'
                viewBox='0 0 24 24'
                stroke='currentColor'
                stroke-width='2'
              >
                <path
                  stroke-linecap='round'
                  stroke-linejoin='round'
                  d='M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207'
                />
              </svg>
            </div>
            <div>{profile?.origin.name}</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Profile;
