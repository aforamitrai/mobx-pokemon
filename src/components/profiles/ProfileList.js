import React from 'react';
import Profile from './Profile';

function ProfileList({ profiles }) {
  return (
    <>
      <div className='grid grid-cols-1 md:grid-cols-3	 gap-3 py-10 '>
        {/* map on list of profile */}
        {profiles.map((profile) => {
          return <Profile profile={profile} />;
        })}
      </div>
    </>
  );
}

export default ProfileList;
