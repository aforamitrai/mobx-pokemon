import axios from 'axios';

const baseUrl = 'https://rickandmortyapi.com/api';

export default axios.create({ baseURL: baseUrl });
